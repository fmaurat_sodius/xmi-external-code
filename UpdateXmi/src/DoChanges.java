import java.io.File;

import com.sodius.mdw.core.CoreException;
import com.sodius.mdw.core.MDWorkbench;
import com.sodius.mdw.core.MDWorkbenchFactory;
import com.sodius.mdw.core.model.MDWList;
import com.sodius.mdw.core.model.Metamodel;
import com.sodius.mdw.core.model.Model;
import com.sodius.mdw.metamodel.xml.Document;
import com.sodius.mdw.metamodel.xml.Element;
import com.sodius.mdw.metamodel.xml.XmlPackage;

public class DoChanges {

    protected MDWorkbench mdw = null;

    public static void main(String[] args) {
        DoChanges doChanges = new DoChanges();
        String pathXMI = args[0];
        String pathXMIResult = pathXMI.replace(".xmi", "_Update.xmi");

        Model xmi = doChanges.readXMI(pathXMI);
        readAndDoChanges(xmi);
        try {
            new File(pathXMIResult).delete();
            xmi.write("XML", pathXMIResult);
            System.out.println("XMI file update has been saved here : " + pathXMIResult);
        } catch (CoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static void readAndDoChanges(Model xmi) {
        if (xmi != null) {
            Document document = xmi.<Document> getInstances(XmlPackage.eINSTANCE.getDocument()).first();
            Element rootElement = document.getRootElement();
            MDWList<Element> contents = rootElement.getContent().<Element> getInstances(XmlPackage.eINSTANCE.getElement());
            Element mainPackage = contents.detect(XmlPackage.eINSTANCE.getElement_TagName(), "uml:Model");
            MDWList<Element> rootPackages = contents.select(XmlPackage.eINSTANCE.getElement_TagName(), "uml:Package");

            // move all packages
            for (Element content : rootPackages) {
                // move
                String nameContent = content.getAttribute("name");
                content.setTagName("packagedElement");
                content.setAttribute("xmi:type", "uml:Package");
                content.setParent(mainPackage);
                System.out.println("Package : " + nameContent + " has been moved successfully");
            }

            contents.removeAll(rootPackages);
            for (Element content : contents) {
                // remove all
                String nameContent = content.getAttribute("name");
                String typeContent = content.getTagName();
                rootElement.getContent().remove(content);
                xmi.remove(content);
                System.out.println("Element has been removed successfully (Type:" + typeContent + " Name:" + nameContent + ")");
            }
        }
    }

    public void shutdown() {
        if (mdw != null) {
            mdw.shutdown();
            mdw = null;
        }
    }

    public Metamodel getMetamodel(String id) throws CoreException {
        return getWorkbench().getMetamodelManager().getMetamodel(id);
    }

    public MDWorkbench getWorkbench() throws CoreException {
        if (mdw == null) {
            mdw = MDWorkbenchFactory.create();
        }
        return mdw;
    }

    private Model readXMI(String path) {
        try {
            Model xmi = getMetamodel("xml").createModel();
            xmi.read("XML", path);
            return xmi;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
